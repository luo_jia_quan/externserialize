﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using SimpleJson;
using UnityEditor;

namespace SerializeExtend
{

    public class JsonDeSerializer : IDeSerializer
    {
        private Dictionary<object, int> _Obj2ID = new Dictionary<object, int>();
        private Dictionary<string, Type> _ID2Type = new Dictionary<string, Type>();
        public Dictionary<string, object> _ID2Object = new Dictionary<string, object>();
        public Dictionary<string, GameObject> _ID2GameObject = new Dictionary<string, GameObject>();
        private JsonObject _Refs;
        private JsonObject _Binds;
        private Dictionary<Type, Dictionary<string, IPropertyOrField>> _Type2Fields = new Dictionary<Type, Dictionary<string, IPropertyOrField>>();

        private static Dictionary<Type, Func<JsonObject,object>> _DefaultDeSerializeTypes = new Dictionary<Type, Func<JsonObject, object>>()
        {
            {typeof(Vector2), o => { return new Vector2(Convert.ToSingle(o["x"]), Convert.ToSingle(o["y"]));} },
            {typeof(Vector3), o => { return new Vector3( Convert.ToSingle(o["x"]),Convert.ToSingle(o["y"]),Convert.ToSingle(o["z"]));} },
            {typeof(Vector4), o => { return new Vector4(Convert.ToSingle(o["x"]),Convert.ToSingle(o["y"]),Convert.ToSingle(o["z"]), Convert.ToSingle(o["w"]));} },
            {typeof(Quaternion), o => { return new Quaternion(Convert.ToSingle(o["x"]),Convert.ToSingle(o["y"]),Convert.ToSingle(o["z"]), Convert.ToSingle(o["w"]));} },
            {typeof(Color), o => { return new Color(Convert.ToSingle(o["r"]),Convert.ToSingle(o["g"]),Convert.ToSingle(o["b"]), Convert.ToSingle(o["a"]));} },
            {typeof(Rect), o => { return new Rect(Convert.ToSingle(o["x"]),Convert.ToSingle(o["y"]),Convert.ToSingle(o["width"]), Convert.ToSingle(o["height"]));} },
        };

        private const string KEY_DATAS = JsonSerializer.KEY_DATAS;
        private const string KEY_DICTIONARY = JsonSerializer.KEY_DICTIONARY;
        private const string KEY_LIST = JsonSerializer.KEY_LIST;
        private const string KEY_HASHSET = JsonSerializer.KEY_HASHSET;
        private const string KEY_STACK = JsonSerializer.KEY_STACK;
        private const string KEY_QUEUE = JsonSerializer.KEY_QUEUE;
        private const string KEY_REF =  JsonSerializer.KEY_REF;
        private const string KEY_TYPE = JsonSerializer.KEY_TYPE;
        private const string KEY_VALUE = JsonSerializer.KEY_VALUE;
        private const string KEY_DIR_TYPE = JsonSerializer.KEY_DIR_TYPE;
        private const string KEY_BIND = JsonSerializer.KEY_BIND;
        private const string KEY_PREFAB_PATH = JsonSerializer.KEY_PREFAB_PATH;
        private const string KEY_SCENE_PATH = JsonSerializer.KEY_SCENE_PATH;
        private static readonly HashSet<string> INTERNAL_KEYS = new HashSet<string>()
        {
            KEY_TYPE
        };

        public static readonly HashSet<string> INTERNAL_SET_KEYS = new HashSet<string>()
        {
            KEY_DICTIONARY,
            KEY_LIST,
            KEY_HASHSET,
            KEY_STACK,
            KEY_QUEUE,
        };

        private static Dictionary<string, Type> String2TypeMap = new Dictionary<string, Type>()
        {
            {KEY_DICTIONARY, typeof(Dictionary<,>) },
            {KEY_LIST, typeof(List<>) },
            {KEY_HASHSET, typeof(HashSet<>) },
            {KEY_STACK, typeof(Stack<>) },
            {KEY_QUEUE, typeof(Queue<>) },
            {JsonSerializer.KEY_GAMEOBJECT, typeof(GameObject)},
            {JsonSerializer.KEY_ANIMATOR, typeof(Animator)},
            {JsonSerializer.KEY_ANIMATION, typeof(Animation)},
            {JsonSerializer.KEY_RENDERER, typeof(Renderer)},
            {JsonSerializer.KEY_MESH, typeof(Mesh)},
        };

        private Dictionary<Type, Action<object, object>> _ICollectionParsers = new Dictionary<Type, Action<object, object>>();
        private Dictionary<string, Type> _Str2Type = new Dictionary<string, Type>();

        public JsonDeSerializer()
        {
            _ICollectionParsers.Add(typeof(Stack), (jsonObjectOrArray, o) => { ParseStackOrQueue(jsonObjectOrArray, o, "Push"); });
            _ICollectionParsers.Add(typeof(Stack<>), (jsonObjectOrArray, o) => { ParseStackOrQueue(jsonObjectOrArray, o, "Push"); });
            _ICollectionParsers.Add(typeof(Queue), (jsonObjectOrArray, o) => { ParseStackOrQueue(jsonObjectOrArray, o, "Enqueue"); });
            _ICollectionParsers.Add(typeof(Queue<>), (jsonObjectOrArray, o) => { ParseStackOrQueue(jsonObjectOrArray, o, "Enqueue"); });
        }

        private void Reset()
        {
            _Obj2ID.Clear();
            _ID2Type.Clear();
            _ID2Object.Clear();
        }

        public T DeSerialize<T>(object obj)
        {
            return (T) DeSerialize(obj, typeof(T));
        }

        public object DeSerialize(object obj, Type type)
        {
            Reset();

            var str = Convert.ToString(obj);
            object deserializedObj = null;
            if (SimpleJson.SimpleJson.TryDeserializeObject(str, out deserializedObj))
            {
                JsonObject jsonObject = deserializedObj as JsonObject;

                object objectValue = null;
                if (jsonObject.TryGetValue(KEY_TYPE, out objectValue))
                {
                    GetTypes(objectValue);
                }

                if (jsonObject.TryGetValue(KEY_BIND, out objectValue))
                {
                    GetBinds(objectValue);
                }

                if (jsonObject.TryGetValue(KEY_REF, out objectValue))
                {
                    GetRefs(objectValue);
                }

                if (jsonObject.TryGetValue(KEY_DATAS, out objectValue))
                {
                    JsonObject jsonValue = objectValue as JsonObject;
                    if (jsonValue != null)
                    {
                        object ID = null;
                        if (jsonValue.TryGetValue(KEY_REF, out ID))
                        {
                            if (_ID2Object.ContainsKey(ID.ToString()))
                            {
                                return _ID2Object[ID.ToString()];
                            }
                        }
                        else
                        {
                            if (_DefaultDeSerializeTypes.ContainsKey(type))
                            {
                                return _DefaultDeSerializeTypes[type](jsonValue);
                            }
                            else
                            {
                                Debug.LogError("_DefaultDeSerializeTypes do not contains valueType parser, valueType = " + type);
                            }
                            //Debug.LogError("_ID2Object Dict do not contains ID, ID = " + ID);
                        }
                    }
                    else
                    {
                        var val = Convert.ChangeType(objectValue, type);
                        return val;
                    }
                }
                else
                {
                    Debug.LogError("Can not get \"Datas\" value!!!");
                    return null;
                }
            }
            else
            {
                Debug.LogError("Invalid JSON string, jsonStr = " + str);
            }

            return null;

        }

        private void GetTypes(object value)
        {
            var typesJsonObject = value as JsonObject;
            foreach (string typeKey in typesJsonObject.Keys)
            {
                string ID = typeKey;
                var val = typesJsonObject[typeKey].ToString();
                Type keyType = GetCustomType(val);
                if (!_ID2Type.ContainsKey(ID))
                {
                    _ID2Type.Add(ID, keyType);
                }
                //Debug.LogError("Types key = " + typeKey + " type = " + typeKey.GetType() +  " value = " + typesJsonObject[typeKey] + " type = " + typesJsonObject[typeKey].GetType());
            }

            Action<List<IPropertyOrField>, Dictionary<string, IPropertyOrField>> addFields =
            (propertiesAndFields, fields) =>
            {
                foreach (var propertiesAndField in propertiesAndFields)
                {
                    var customSerializeFields = propertiesAndField.GetCustomAttributes<CustomSerializeField>();
                    if (customSerializeFields != null)
                    {
                        foreach (var customSerializeField in customSerializeFields)
                        {
                            string keyName = customSerializeField.KeyName != null ? customSerializeField.KeyName : propertiesAndField.Name;
                            if (!fields.ContainsKey(keyName))
                            {
                                fields.Add(keyName, propertiesAndField);
                            }
                        }
                    }
                    if (!fields.ContainsKey(propertiesAndField.Name))
                    {
                        fields.Add(propertiesAndField.Name, propertiesAndField);
                    }
                }
            };

            foreach (var temp in _ID2Type)
            {
                Type type = temp.Value;
                Dictionary<string, IPropertyOrField> fields = new Dictionary<string, IPropertyOrField>();
                if (!_Type2Fields.ContainsKey(type))
                {
                    _Type2Fields.Add(type, fields);
//                    var propertiesAndFields = Reflections.GetPropertiesAndFields(type, null, BindingFlags.Instance | BindingFlags.Public);
//                    addFields(propertiesAndFields, fields);
                    var propertiesAndFields = Reflections.GetPropertiesAndFields<CustomSerializeField>(type, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    addFields(propertiesAndFields, fields);
                }
            }
        }

        private void GetBinds(object value)
        {
            var bindJsonObject = value as JsonObject;
            _Binds = bindJsonObject;
            foreach (var bindKey in bindJsonObject.Keys)
            {
                string ID = bindKey;
                _GetOrCreateGameObject(ID);
            }
        }

        private GameObject _GetOrCreateGameObject(string ID)
        {
            if (!_ID2GameObject.ContainsKey(ID))
            {
                var jsonObj = _Binds[ID] as JsonObject;
                object path = null;
                if (jsonObj.TryGetValue(KEY_PREFAB_PATH, out path))
                {
#if UNITY_EDITOR
                    var prefabObj = AssetDatabase.LoadAssetAtPath<GameObject>(path.ToString());
                    var cloneObj = GameObject.Instantiate(prefabObj);
                    _ID2GameObject.Add(ID, cloneObj);
#endif
                }
                else if (jsonObj.TryGetValue(KEY_SCENE_PATH, out path))
                {
                    var sceneObj = GameObject.Find(path.ToString());
                    //todo fin gameobject with a path
                    //var sceneObj = GameObjectFindUtils.Find(path.ToString());
                    _ID2GameObject.Add(ID, sceneObj);
                }
            }
            return _ID2GameObject[ID];
        }

        private void GetRefs(object value)
        {
            var refJsonObject = value as JsonObject;
            _Refs = refJsonObject;
            foreach (string refKey in refJsonObject.Keys)
            {
                string ID = refKey;
                _GetOrCreateInstance(ID);
            }
        }

        private object _GetOrCreateInstance(string ID)
        {
            if (!_ID2Object.ContainsKey(ID))
            {
                var jsonObj = _Refs[ID] as JsonObject;
                object typeID = null;
                if (jsonObj.TryGetValue(KEY_TYPE, out typeID))
                {
                    var type = _ID2Type[typeID.ToString()];
                    object obj = null;
                    if (type.IsArray)
                    {
                        int count = 1;
                        if(jsonObj.ContainsKey(KEY_VALUE))
                        {
                            var jsonArray = jsonObj[KEY_VALUE] as JsonArray;
                            count = jsonArray.Count;
                        }
                        obj = Array.CreateInstance(type.GetElementType(), count);
                    }
                    else
                    {
                        obj = Activator.CreateInstance(type);
                    }
                    _ID2Object.Add(ID, obj);
                    _InitField(jsonObj, obj);
                }
                else
                {
                    Debug.LogError("jsonObject do not contains \"$Type\", jsonObj = " + jsonObj.ToString());
                }
            }
            return _ID2Object[ID];
        }

        private void _InitField(object jsonObjectOrArray, object o)
        {
            var type = o.GetType();
            bool isDictionary = typeof(IDictionary).IsAssignableFrom(type);
            if (isDictionary)
            {
                var method = type.GetMethod("Add");
                var jsonObject = jsonObjectOrArray as JsonObject;
                foreach (var key in jsonObject.Keys)
                {
                    if(INTERNAL_KEYS.Contains(key)) continue;
                    Type keyType = type.GenericTypeArguments[0];
                    Type valueType = type.GenericTypeArguments[1];

                    object keyVal = ParseObject(key, keyType);
                    object valueVal = ParseObject(jsonObject[key], valueType);
                    method.Invoke(o, new[] { keyVal , valueVal });
                }
            }
            else if(type.IsArray)
            {
                var jsonObject = jsonObjectOrArray as JsonObject;
                if(jsonObject.ContainsKey(KEY_VALUE))
                {
                    var jsonArray = jsonObject[KEY_VALUE] as JsonArray;
                    if (jsonArray != null)
                    {
                        Array array = o as Array;
                        for(int i = 0; i < jsonArray.Count; i++)
                        {
                            object element = jsonArray[i];
                            var valueType = type.GetElementType();
                            object val = ParseObject(element, valueType);
                            array.SetValue(val, i);
                        }
                    }
                    else
                    {
                        Debug.LogError("jsonObject[KEY_VALUE] can not convert to JsonArray, jsonObject[KEY_VALUE] = " + jsonObject[KEY_VALUE]);
                    }
                }
            }
            //else if (typeof(IList).IsAssignableFrom(type))
            //{
            //    ParseListOrHashSet(jsonObjectOrArray, o);
            //}
            else if (typeof(ICollection).IsAssignableFrom(type) || typeof(IEnumerable).IsAssignableFrom(type))
            {
                Type valueType = type;
                if (type.IsGenericType)
                {
                    valueType = type.GetGenericTypeDefinition();
                }
                if (_ICollectionParsers.ContainsKey(valueType))
                {
                    _ICollectionParsers[valueType](jsonObjectOrArray, o);
                }
                else
                {
                    ParseListOrHashSet(jsonObjectOrArray, o);
                }
            }
            else
            {
                var propertyAndFieleds = _Type2Fields[type];
                var jsonObject = jsonObjectOrArray as JsonObject;
                foreach (var key in jsonObject.Keys)
                {
                    if (propertyAndFieleds.ContainsKey(key))
                    {
                        var fieldAndProperty = propertyAndFieleds[key];
                        Type fieldAndPropertyType = fieldAndProperty.GetFieldOrPropertyType();
                        object val = jsonObject[key];
                        var nestedJsonObject = jsonObject[key] as JsonObject;
                        if (nestedJsonObject != null)
                        {
                            val = ParseObject(nestedJsonObject, fieldAndPropertyType);
                        }
                        else
                        {
                            if (fieldAndPropertyType.IsEnum)
                            {
                                val = Enum.Parse(fieldAndPropertyType, val.ToString());
                            }
                            else
                            {
                                val = Convert.ChangeType(val, fieldAndPropertyType);
                            }
                        }
                        fieldAndProperty.SetValue(o, val);
                    }
                }
            }
        }

        private void ParseListOrHashSet(object jsonObjectOrArray, object o)
        {
            var jsonObject = jsonObjectOrArray as JsonObject;
            if (jsonObject.ContainsKey(KEY_VALUE))
            {
                var type = o.GetType();
                var valueType = type.GenericTypeArguments[0];
                var method = type.GetMethod("Add");
                if (method == null)
                {
                    Debug.LogError("this type do not contains method, method name = \"Add\"");
                    return;
                }
                var jsonArray = jsonObject[KEY_VALUE] as JsonArray;
                if (jsonArray != null)
                {
                    for (int i = 0; i < jsonArray.Count; i++)
                    {
                        object element = jsonArray[i];
                        object val = ParseObject(element, valueType);
                        method.Invoke(o, new object[] { val });
                    }
                }
                else
                {
                    Debug.LogError("jsonObject[KEY_VALUE] can not convert to JsonArray, jsonObject[KEY_VALUE] = " + jsonObject[KEY_VALUE]);
                }
            }
        }

        private object ParseObject(object rawObject, Type valueType)
        {
            JsonObject jsonObject = rawObject as JsonObject;
            object valueVal = null;
            if (jsonObject != null)
            {
                if (jsonObject.ContainsKey(KEY_REF))
                {
                    var refID = jsonObject[KEY_REF] as string;
                    valueVal = _GetOrCreateInstance(refID);
                }
                else if(jsonObject.ContainsKey(KEY_DIR_TYPE))
                {
                    var typeID = jsonObject[KEY_DIR_TYPE];
                    valueVal = _ID2Type[typeID.ToString()];
                }
                else if (jsonObject.ContainsKey(KEY_BIND))
                {
                    var bindID = jsonObject[KEY_BIND];
                    valueVal = _ID2GameObject[bindID.ToString()];
                }
                else
                {
                    if (_DefaultDeSerializeTypes.ContainsKey(valueType))
                    {
                        return _DefaultDeSerializeTypes[valueType](jsonObject);
                    }
                    else
                    {
                        Debug.LogError("_DefaultDeSerializeTypes do not contains valueType parser, valueType = " + valueType);
                    }
                }
            }
            else
            {
                if (!valueType.IsValueType)
                {
                    object json = null;
                    if (SimpleJson.SimpleJson.TryDeserializeObject(rawObject.ToString(), out json))
                    {
                        valueVal = ParseObject(json, valueType);
                    }
                    else
                    {
                        //Debug.LogError("Invalid Json String, json = " + rawObject.ToString());
                        valueVal = Convert.ChangeType(rawObject, valueType);
                    }
                }
                else
                {
                    valueVal = Convert.ChangeType(rawObject, valueType);
                }
            }
            return valueVal;
        }

        private void ParseStackOrQueue(object jsonObjectOrArray, object o, string methodName)
        {
            var jsonObject = jsonObjectOrArray as JsonObject;
            if (jsonObject.ContainsKey(KEY_VALUE))
            {
                var type = o.GetType();
                var method = type.GetMethod(methodName);
                var methodParameters = method.GetParameters();
                Type valueType = methodParameters[0].ParameterType == typeof(object) ? typeof(object) : methodParameters[0].ParameterType;
                var jsonArray = jsonObject[KEY_VALUE] as JsonArray;
                if (jsonArray != null)
                {
                    for (int i = 0; i < jsonArray.Count; i++)
                    {
                        object element = jsonArray[i];
                        object val = ParseObject(element, valueType);
                        method.Invoke(o, new object[] { val });
                    }
                }
                else
                {
                    Debug.LogError("jsonObject[KEY_VALUE] can not convert to JsonArray, jsonObject[KEY_VALUE] = " + jsonObject[KEY_VALUE]);
                }
            }
        }

        private Type GetCustomType2(string val)
        {
            int finishedIndex = 0;
            Stack<int> waitingTypesIndex = new Stack<int>();
            List<Type> typeList = new List<Type>();
            int currentTypeIndex = 0;
            Action<Type> push = (type) =>
            {
                if (currentTypeIndex == typeList.Count)
                {
                    typeList.Add(type);
                }
                else
                {
                    typeList[currentTypeIndex] = type;
                }
                currentTypeIndex++;
            };
            Action<int> pop = (count) => { currentTypeIndex -= count; };

            for (int i = 0; i < val.Length; i++)
            {
                char c = val[i];
                if (c == ' ')
                {
                    continue;
                }
                else if (c == '[')
                {
                    Type type = GetType(val.Substring(finishedIndex, i - finishedIndex));
                    finishedIndex = i + 1;
                    push(type);
                    waitingTypesIndex.Push(currentTypeIndex - 1);
                }
                else if (c == ']')
                {
                    Type type = GetType(val.Substring(finishedIndex, i - finishedIndex));
                    finishedIndex = i + 1;

                    int waitingTypeIndex = waitingTypesIndex.Pop();
                    int argumentLength = currentTypeIndex - waitingTypeIndex - 1;
                    if (type != null)
                    {
                        argumentLength++;
                    }

                    Type[] typeArguments = new Type[argumentLength];
                    for (int j = 0; j < typeArguments.Length - 1; j++)
                    {
                        typeArguments[j] = typeList[j + waitingTypeIndex + 1];
                    }
                    typeArguments[typeArguments.Length - 1] = type ?? typeList[typeArguments.Length + waitingTypeIndex];

                    Type currentType = typeList[waitingTypeIndex];
                    currentType = currentType.MakeGenericType(typeArguments);
                    pop(currentTypeIndex - waitingTypeIndex);
                    push(currentType);
                }
                else if (c == JsonSerializer.TYPE_SPLIT_SYMBOL)
                {
                    if (i == finishedIndex)
                    {
                        finishedIndex = i + 1;
                        continue;
                    }
                    push(GetType(val.Substring(finishedIndex, i - finishedIndex)));
                    finishedIndex = i + 1;
                }

            }
            return typeList[currentTypeIndex - 1];
        }

        private Type GetType(string typeStr)
        {
            Type type = null;
            typeStr = typeStr.Trim();
            if (String2TypeMap.ContainsKey(typeStr))
            {
                type = String2TypeMap[typeStr];
            }
            else
            {
                type = Type.GetType(typeStr);
            }
            return type;

        }

        private Type GetCustomType(string val)
        {
            if (String2TypeMap.ContainsKey(val))
            {
                return String2TypeMap[val];
            }

            if (_Str2Type.ContainsKey(val))
            {
                return _Str2Type[val];
            }
            bool isFind = false;
            foreach (var key in INTERNAL_SET_KEYS)
            {
                if (val.StartsWith(key))
                {
                    isFind = true;
                    _Str2Type[val] = GetCustomType2(val);
                    break;
                }
            }

            if (!isFind)
            {
                _Str2Type[val] = Type.GetType(val);
                if (_Str2Type[val] == null)
                {
                    Debug.LogError("this string can not convert to type, string val = " + val);
                }
                

            }
            return _Str2Type[val];
        }

        private Assembly[] _AssemblyArray;

    }
}
