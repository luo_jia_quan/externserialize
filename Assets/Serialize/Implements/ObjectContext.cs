﻿
using System.Collections.Generic;

namespace SerializeExtend
{
    public class ObjectContext
    {
        private Dictionary<object, object> SerializedObjDict = new Dictionary<object, object>();
        private List<object> SerializedList = new List<object>();

        public void AddSerializedObj(object obj)
        {
            if (!SerializedList.Contains(obj))
            {
                SerializedList.Add(obj);
            }
        }

        public bool ContainsSerializedObj(object obj)
        {
            return SerializedList.Contains(obj);
        }

        public void AddSerializedObj(object key, object val)
        {
            if(!SerializedObjDict.ContainsKey(key))
            {
                SerializedObjDict.Add(key, val);
            }
        }

        public bool HasSerializedObj(object key)
        {
            return SerializedObjDict.ContainsKey(key);
        }

        public object GetSerializedObj(object key)
        {
            if (HasSerializedObj(key))
            {
                return SerializedObjDict[key];
            }

            return null;
        }


    }


}
