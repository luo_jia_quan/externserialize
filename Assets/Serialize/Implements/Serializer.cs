﻿using System;

namespace SerializeExtend
{
    public class Serializer
    {
        public static ISerializer CurrentSerializer;
        public static IDeSerializer CurrentDeSerializer;

        public Serializer(ISerializer currentSerializer, IDeSerializer currentDeSerializer)
        {
            CurrentSerializer = currentSerializer;
            CurrentDeSerializer = currentDeSerializer;

            if (CurrentSerializer == null)
            {
                CurrentSerializer = new JsonSerializer();
            }

            if (CurrentDeSerializer == null)
            {
                CurrentDeSerializer = new JsonDeSerializer();
            }
        }

        public object Serialize(object obj)
        {
            return CurrentSerializer.Serialize(obj);
        }

        public object DeSerialize(object obj, Type type)
        {
            return CurrentDeSerializer.DeSerialize(obj, type);
        }

    }
}
