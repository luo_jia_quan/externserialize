﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SerializeExtend
{
    public class JsonSerializer : ISerializer
    {
        private static HashSet<Type> _RawTypes = new HashSet<Type>()
        {
            typeof(byte),
            typeof(sbyte),
            typeof(int),
            typeof(uint),
            typeof(short),
            typeof(ushort),
            typeof(long),
            typeof(ulong),
            typeof(float),
            typeof(double),
            typeof(decimal),
        };

        private static Dictionary<Type, Func<object, string>> _DefaultSerializeTypes = new Dictionary<Type, Func<object, string>>()
        {
            {typeof(Vector2), o => {Vector2 v = (Vector2) o; return "{\"x\":" + v.x +  ",\"y\":" + v.y + "}"; }},
            { typeof(Vector3), o => {Vector3 v = (Vector3) o; return "{\"x\":" + v.x + ",\"y\":" + v.y + ",\"z\":" + v.z + "}";}},
            { typeof(Vector4), o => {Vector4 v = (Vector4) o; return "{\"x\":" + v.x + ",\"y\":" + v.y + ",\"z\":" + v.z + ",\"w\":" + v.w + "}";}},
            { typeof(Quaternion), o => {Quaternion q = (Quaternion) o; return "{\"x\":" + q.x + ",\"y\":" + q.y + ",\"z\":" + q.z + ",\"w\":" + q.w + "}";}},
            { typeof(Color), o => {Color c = (Color) o; return "{\"r\":" + c.r + ",\"g\":" + c.g + ",\"b\":" + c.b + ",\"a\":" + c.a + "}";}},
            { typeof(Rect), o => {Rect r = (Rect) o; return "{\"x\":" + r.x + ",\"y\":" + r.y + ",\"width\":" + r.width + ",\"height\":" + r.height + "}";}},
            { typeof(string), o => "\"" + o + "\"" },
            { typeof(char), o => "\"" + o + "\""},
            { typeof(bool), o => "\"" + o + "\"" },
        };

        //Short cut
        public const string KEY_DATAS = "Datas";
        public const string KEY_DICTIONARY = "Dict";
        public const string KEY_LIST = "List";
        public const string KEY_HASHSET = "HashSet";
        public const string KEY_STACK = "Stack";
        public const string KEY_QUEUE = "Queue";
        public const string KEY_GAMEOBJECT = "$Go";
        public const string KEY_ANIMATOR = "$Atr";
        public const string KEY_ANIMATION = "$Amn";
        public const string KEY_RENDERER = "$Rdr";
        public const string KEY_MESH = "$Msh";

        private Dictionary<Type, string> _Type2ShortString = new Dictionary<Type, string>()
        {
            {typeof(GameObject), KEY_GAMEOBJECT},
            {typeof(Animator), KEY_ANIMATOR},
            {typeof(Animation), KEY_ANIMATION},
            {typeof(Renderer), KEY_RENDERER},
            {typeof(Mesh), KEY_MESH},

        };

        private Dictionary<Type, Dictionary<string, IPropertyOrField>> _Type2Fields = new Dictionary<Type, Dictionary<string, IPropertyOrField>>();
        private Dictionary<Type, int> _Type2ID = new Dictionary<Type, int>();
        private Dictionary<object, int> _Obj2ID = new Dictionary<object, int>();
        private Dictionary<object, string> _Obj2Json = new Dictionary<object, string>();
        private Dictionary<Type, string> _Type2String = new Dictionary<Type, string>();
        private Dictionary<object, int> _GameObject2ID = new Dictionary<object, int>();


        public const string KEY_REF = "$Ref";
        public const string KEY_TYPE = "$Type";
        public const string KEY_VALUE = "$Value";
        public const string KEY_DIR_TYPE = "$DType";
        public const string KEY_BIND = "$Bind";
        public const string KEY_PREFAB_PATH = "$Path";
        public const string KEY_SCENE_PATH = "Path";
        public const char TYPE_SPLIT_SYMBOL = ';';
        private const int _TypeNameLimitLength = 20;

        private int count = 0;
        private int _TypeIDCount = 0;
        private int _ObjIDCount = 0;
        private int _GameObjectIDCount = 0;

        private void Reset()
        {
            _Type2ID.Clear();
            _Obj2ID.Clear();
            _Obj2Json.Clear();

            _TypeIDCount = 0;
            _ObjIDCount = 0;
            _GameObjectIDCount = 0;
        }

        public object Serialize(object obj)
        {
            Reset();
            var serializedData = SerializeData(obj);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{");
            stringBuilder.Append("\"").Append(KEY_DATAS).Append("\":").Append(serializedData);
            if (_TypeIDCount > 0)
            {
                stringBuilder.Append(", ");
                var serializedTypes = SerializeType(); //SerializeType(obj);
                stringBuilder.Append("\"").Append(KEY_TYPE).Append("\":{").Append(serializedTypes).Append("}");
            }
            if (_ObjIDCount > 0)
            {
                stringBuilder.Append(", ");
                var serializedInstances = SerializeInstance();
                stringBuilder.Append("\"").Append(KEY_REF).Append("\":{").Append(serializedInstances).Append("}");
            }

            if (_GameObjectIDCount > 0)
            {
                stringBuilder.Append(", ");
                var serializeGameObjects = SerializeGameObject();
                stringBuilder.Append("\"").Append(KEY_BIND).Append("\":{").Append(serializeGameObjects).Append("}");
            }

            stringBuilder.Append("}");
            return stringBuilder;
        }

        private object SerializeGameObject()
        {
            StringBuilder stringBuilder = new StringBuilder();
            bool add = false;
            foreach (var temp in _GameObject2ID)
            {
                if (add)
                {
                    stringBuilder.Append(", ");
                }
                add = true;
                stringBuilder.Append("\"").Append(temp.Value).Append("\":").Append(_Obj2Json[temp.Key]);
            }

            return stringBuilder;
        }

        private object SerializeInstance()
        {
            StringBuilder stringBuilder = new StringBuilder();
            bool add = false;
            foreach (var temp in _Obj2ID)
            {
                if (add)
                {
                    stringBuilder.Append(", ");
                }
                stringBuilder.Append("\"").Append(temp.Value).Append("\":").Append(_Obj2Json[temp.Key]);
                add = true;
            }

            return stringBuilder;
        }

        private object SerializeType()
        {
            StringBuilder stringBuilder = new StringBuilder();
            bool add = false;
            foreach (var temp in _Type2ID)
            {
                if (add)
                {
                    stringBuilder.Append(", ");
                }
                var type = temp.Key;
                bool isEnumable = typeof(IEnumerable).IsAssignableFrom(type);
                if (isEnumable && !type.IsArray)
                {
                    stringBuilder.Append("\"").Append(temp.Value).Append("\":\"").Append(GetCustomSerializeType(type)).Append("\"");
                }
                else
                {
                    string typeName = GetTypeName(type);
                    stringBuilder.Append("\"").Append(temp.Value).Append("\":\"").Append(typeName).Append("\"");

                    //string typeFullName = type.FullName;
                    //if (Type.GetType(typeFullName) == null)
                    //{
                    //    string typeAssemblyName = type.Assembly.GetName(false).Name;
                    //    stringBuilder.Append("\"").Append(temp.Value).Append("\":\"").Append(typeFullName).Append(",").Append(typeAssemblyName).Append("\"");
                    //}
                    //else
                    //{
                    //    stringBuilder.Append("\"").Append(temp.Value).Append("\":\"").Append(typeFullName).Append("\"");
                    //}
                }
                add = true;
            }
            return stringBuilder;
        }

        private string GetCustomSerializeType(Type type)
        {
            StringBuilder stringBuilder = new StringBuilder();
            bool isEnumable = typeof(IEnumerable).IsAssignableFrom(type);
            bool isDictionary = typeof(IDictionary).IsAssignableFrom(type);
            bool isList = typeof(IList).IsAssignableFrom(type);
            bool isHashSet = type.IsGenericType && type.GetGenericTypeDefinition() == typeof(HashSet<>);
            bool isGenericStack = type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Stack<>);
            bool isGenericQueue = type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Queue<>);
            if (isEnumable)
            {
                if (isDictionary)
                {
                    var keyType = GetCustomSerializeType(type.GenericTypeArguments[0]);
                    var valueType = GetCustomSerializeType(type.GenericTypeArguments[1]);
                    stringBuilder.Append(KEY_DICTIONARY).Append("[").Append(keyType).Append(TYPE_SPLIT_SYMBOL).Append(valueType).Append("]");
                }
                else if (isList)
                {
                    var valueType = GetCustomSerializeType(type.GenericTypeArguments[0]);
                    stringBuilder.Append(KEY_LIST).Append("[").Append(valueType).Append("]");
                }
                else if (isHashSet)
                {
                    var valueType = GetCustomSerializeType(type.GenericTypeArguments[0]);
                    stringBuilder.Append(KEY_HASHSET).Append("[").Append(valueType).Append("]");
                }
                else if (isGenericStack)
                {
                    var valueType = GetCustomSerializeType(type.GenericTypeArguments[0]);
                    stringBuilder.Append(KEY_STACK).Append("[").Append(valueType).Append("]");
                }
                else if(isGenericQueue)
                {
                    var valueType = GetCustomSerializeType(type.GenericTypeArguments[0]);
                    stringBuilder.Append(KEY_QUEUE).Append("[").Append(valueType).Append("]");
                }
                else
                {
                    stringBuilder.Append(GetTypeName(type));
                }
            }
            else
            {
                stringBuilder.Append(GetTypeName(type));
            }
            return stringBuilder.ToString();
        }

        private object SerializeData(object obj)
        {
            if (obj == null) return null;
//            if (count > 500) return null;
//            count++;

            var type = obj.GetType();
            Type typeInfo = null;
            if (!type.IsValueType && !(obj is string))  //ref types
            {
                if (obj is Type)
                {
                    typeInfo = Type.GetType((obj as Type).FullName);
                }
                if (typeInfo == null)
                {
                    typeInfo = type;
                }

                if (!_Type2ID.ContainsKey(typeInfo))
                {
                    _TypeIDCount++;
                    _Type2ID.Add(typeInfo, _TypeIDCount);
                }

                if (_GameObject2ID.ContainsKey(obj))
                {
                    return string.Format("{{\"{0}\":\"{1}\"}}", KEY_BIND, _GameObject2ID[obj]);
                }
                if (_Obj2ID.ContainsKey(obj))
                {
                    return string.Format("{{\"{0}\":\"{1}\"}}", KEY_REF, _Obj2ID[obj]);
                }
            }

            if (obj is Type)
            {
                return string.Format("{{\"{0}\":\"{1}\"}}", KEY_DIR_TYPE, _Type2ID[typeInfo]);
            }
            else if (_RawTypes.Contains(type))
            {
                return "\"" + obj + "\"";
            }
            else if (_DefaultSerializeTypes.ContainsKey(type))
            {
                object serializedValue = _DefaultSerializeTypes[type](obj);
                return serializedValue;
            }
            else
            {
                bool isEnumable = typeof(IEnumerable).IsAssignableFrom(type);
                bool isDictionary = typeof(IDictionary).IsAssignableFrom(type);

                if (type.IsEnum)
                {
                    return Convert.ToInt32(obj);
                }
                else if (isDictionary)
                {
                    _ObjIDCount++;
                    _Obj2ID.Add(obj, _ObjIDCount);

                    IDictionary dictionary = (IDictionary)obj;
                    StringBuilder stringBuilder = new StringBuilder("{");
                    int value = 0;
                    if (!_Type2ID.TryGetValue(type, out value))
                    {
                        Debug.LogError("_Type2ID do not contains Key, key type = " + type.FullName);
                    }
                    stringBuilder.Append(string.Format("\"{0}\":\"{1}\"", KEY_TYPE, value));
                    if (dictionary.Count > 0)
                    {
                        stringBuilder.Append(", ");
                    }

                    bool added = false;
                    foreach (DictionaryEntry dictionaryEntry in dictionary)
                    {
                        var serializeKey = SerializeData(dictionaryEntry.Key);
                        var serializeValue = SerializeData(dictionaryEntry.Value);
                        if (serializeKey == null || serializeValue == null)
                        {
                            continue;
                        }

                        if (added)
                        {
                            stringBuilder.Append(", ");
                        }

                        if (!dictionaryEntry.Key.GetType().IsValueType && !(dictionaryEntry.Key is string))
                        {
                            serializeKey = "\"" + serializeKey.ToString().Replace("\"", "\\\"") + "\"";
                        }
                        stringBuilder.Append(string.Format("{0}:{1}", serializeKey, serializeValue));

                        added = true;
                    }
                    stringBuilder.Append("}");
                    _Obj2Json[obj] = stringBuilder.ToString();
                    return string.Format("{{\"{0}\":\"{1}\"}}", KEY_REF, _Obj2ID[obj]);
                }
                else if (isEnumable)
                {
                    _ObjIDCount++;
                    _Obj2ID.Add(obj, _ObjIDCount);

                    IEnumerable enumerable = (IEnumerable)obj;
                    StringBuilder stringBuilder = new StringBuilder("{");
                    int value = 0;
                    if (!_Type2ID.TryGetValue(type, out value))
                    {
                        Debug.LogError("_Type2ID do not contains Key, key type = " + GetTypeName(type));
                    }
                    stringBuilder.Append(string.Format("\"{0}\":\"{1}\"", KEY_TYPE, value));
                    stringBuilder.Append(", \"").Append(KEY_VALUE).Append("\":");
                    stringBuilder.Append("[");
                    bool added = false;
                    foreach (var ele in enumerable)
                    {
                        var serialized = SerializeData(ele);
                        if (serialized == null)
                        {
                            continue;
                        }

                        if (added)
                        {
                            stringBuilder.Append(", ");
                        }

                        stringBuilder.Append(serialized);
                        added = true;
                    }
                    stringBuilder.Append("]");
                    stringBuilder.Append("}");
                    _Obj2Json[obj] = stringBuilder.ToString();
                    return string.Format("{{\"{0}\":\"{1}\"}}", KEY_REF, _Obj2ID[obj]);
                }
                else
                {
                    Action<List<IPropertyOrField>, Dictionary<string, IPropertyOrField>> addFields =
                    (propertiesAndFields, fields) =>
                    {
                        foreach (var propertiesAndField in propertiesAndFields)
                        {
                            var customSerializeFields = propertiesAndField.GetCustomAttributes<CustomSerializeField>();
                            if (customSerializeFields != null)
                            {
                                foreach (var customSerializeField in customSerializeFields)
                                {
                                    string keyName = customSerializeField.KeyName != null ? customSerializeField.KeyName : propertiesAndField.Name;
                                    if (!fields.ContainsKey(keyName))
                                    {
                                        fields.Add(keyName, propertiesAndField);
                                    }
                                }
                            }
                            if (!fields.ContainsKey(propertiesAndField.Name))
                            {
                                fields.Add(propertiesAndField.Name, propertiesAndField);
                            }
                        }
                    };

                    StringBuilder stringBuilder = new StringBuilder("{");
                    //bind Implements
                    if (type == typeof(GameObject))
                    {
                        _GameObjectIDCount++;
                        _GameObject2ID.Add(obj, _GameObjectIDCount);

                        //int typeID = 0;
                        //if (!_Type2ID.TryGetValue(type, out typeID))
                        //{
                        //    Debug.LogError("_Type2ID do not contains Key, key type = " + type.FullName);
                        //}
                        //stringBuilder.Append(string.Format("\"{0}\":\"{1}\"", KEY_TYPE, typeID));
                        GameObject gameObject = obj as GameObject;
                        if (PrefabUtility.IsPartOfAnyPrefab(gameObject))
                        {
                            Object prefab = PrefabUtility.GetCorrespondingObjectFromOriginalSource(gameObject);
                            string path = AssetDatabase.GetAssetPath(prefab);
                            stringBuilder.Append(string.Format("\"{0}\":\"{1}\"", KEY_PREFAB_PATH, path));
                        }
                        else
                        {
                            string scenePath = gameObject.transform.GetFullHierarchyPath();
                            stringBuilder.Append(string.Format("\"{0}\":\"{1}\"", KEY_SCENE_PATH, scenePath));
                        }

                        stringBuilder.Append("}");
                        _Obj2Json[obj] = stringBuilder.ToString();
                        
                        return string.Format("{{\"{0}\":\"{1}\"}}", KEY_BIND, _GameObject2ID[obj]);
                    }

                    //Ref Implements
                    _ObjIDCount++;
                    _Obj2ID.Add(obj, _ObjIDCount);

                    Dictionary<string, IPropertyOrField> fieldsDict = new Dictionary<string, IPropertyOrField>();
                    if (_Type2Fields.ContainsKey(type))
                    {
                        fieldsDict = _Type2Fields[type];
                    }
                    else
                    {
                        _Type2Fields.Add(type, fieldsDict);
//                        var propertiesAndFields = Reflections.GetPropertiesAndFields(type, null, BindingFlags.Instance | BindingFlags.Public);
//                        addFields(propertiesAndFields, fieldsDict);
                        var propertiesAndFields = Reflections.GetPropertiesAndFields<CustomSerializeField>(type, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                        addFields(propertiesAndFields, fieldsDict);
                    }

                    bool added = false;

                    int value = 0;
                    if (!_Type2ID.TryGetValue(type, out value))
                    {
                        Debug.LogError("_Type2ID do not contains Key, key type = " + type.FullName);
                    }
                    stringBuilder.Append(string.Format("\"{0}\":\"{1}\"", KEY_TYPE, value));
                    added = true;
                    foreach (var propertyOrField in fieldsDict)
                    {
                        var fieldValue = propertyOrField.Value;
                        var serialized = SerializeData(fieldValue.GetValue(obj));
                        if (serialized == null)
                        {
                            continue;
                        }

                        if (added)
                        {
                            stringBuilder.Append(", ");
                        }

                        var customSerializeField = fieldValue.GetCustomAttribute<CustomSerializeField>();
                        string keyName = customSerializeField != null && !string.IsNullOrEmpty(customSerializeField.KeyName) ? customSerializeField.KeyName : fieldValue.Name;
                        stringBuilder.Append("\"").Append(keyName).Append("\":").Append(serialized);
                        //added = true;
                    }

                    stringBuilder.Append("}");
                    _Obj2Json[obj] = stringBuilder.ToString();

                    return string.Format("{{\"{0}\":\"{1}\"}}", KEY_REF, _Obj2ID[obj]);
                }
            }
        }

        private string GetTypeName(Type type)
        {
            if (_Type2ShortString.ContainsKey(type))
            {
                return _Type2ShortString[type];
            }

            if (_Type2String.ContainsKey(type))
            {
                return _Type2String[type];
            }

            string result;
            string typeFullName = type.FullName;
            if (Type.GetType(typeFullName) == null)
            {
                string typeAssemblyName = type.Assembly.GetName(false).Name;
                result = string.Format("{0},{1}", typeFullName, typeAssemblyName);
            }
            else
            {
                result = typeFullName;
            }
            if (result.Length > _TypeNameLimitLength)
            {
                Debug.LogWarning("this type name length is more than " + _TypeNameLimitLength + " characters, you can add this type " +
                                 "to _Type2ShortString dict with shortcuts replace, " + "in order to shorten json string." + " this Type = " + result);
            }
            _Type2String.Add(type, result);
            return result;
        }

    }
}
