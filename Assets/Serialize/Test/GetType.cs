﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GetTypeClass
{
    private static Dictionary<string, Type> String2TypeMap = new Dictionary<string, Type>()
        {
            {"Dictionary", typeof(Dictionary<,>) },
            {"List", typeof(List<>) },
            {"HashSet", typeof(HashSet<>) },
            {"Stack", typeof(Stack<>) },
            {"Queue", typeof(Queue<>) },
        };

    private static Type RecursionGetCustomType2(string val)
    {
        val = val.Trim();
        if (String2TypeMap.ContainsKey(val))
        {
            return String2TypeMap[val];
        }
        Type type = Type.GetType(val);
        if (type != null)
        {
            return type;
        }

        string currentTypeStr = null;
        if (val.Contains("["))
        {
            currentTypeStr = val.Substring(0, val.IndexOf('['));
            var currentType = RecursionGetCustomType2(currentTypeStr);
            var argumentNums = currentType.IsGenericType ? currentType.GetGenericArguments().Length : 0;
            if (argumentNums > 0)
            {
                int firstIndexOfLeftSquare = val.IndexOf('[');
                var subStr = val.Substring(firstIndexOfLeftSquare + 1, val.LastIndexOf(']') - firstIndexOfLeftSquare - 1);
                string[] nextStr = new string[argumentNums];


                Type[] arguments = new Type[argumentNums];
                for (int i = 0; i < argumentNums; i++)
                {
                    //Debug.LogError("nextStr[i] = " + nextStr[i]);
                    arguments[i] = RecursionGetCustomType2(nextStr[i]);
                }

                return currentType.MakeGenericType(arguments);
            }
            else
            {
                Debug.LogError("argumentNums == 0, type = " + currentType);
            }
        }
        Debug.LogError("this typeString can not parse, typeStr = " + val);
        return null;
    }

}
