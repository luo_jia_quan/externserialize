﻿using System;
using System.Collections.Generic;
using SimpleJson;
using UnityEditor;
using UnityEngine;
using JsonObject = SimpleJson.JsonObject;

namespace SerializeExtend
{
    class TestCustomSerialize
    {
        private static object _SerializedJson = null;

        [MenuItem("CustomSerialize/TestSerialize")]
        static void TestSerialize()
        {
            MutualReferenceTest mutualReference = new MutualReferenceTest("china", null);
            Cat cat = new Cat("Hello", 20);
            Dog dog = new Dog("Doller");
            cat.Dog = dog;
            dog.Cat = cat;
            //mutualReference._Pets = new IAnimal[]{ new Cat("Hello", 20), new Dog("World"), };
            mutualReference._Pets = new IAnimal[]{ cat, cat, };
            mutualReference._FirstAnimal = dog;
            MutualReferenceTest22 aaa = new AAA();
            aaa._Num = 200;
            aaa._Mutual = null;
            mutualReference.SetMutaual(aaa);
            mutualReference._Implement = new MyImplement();
            mutualReference._SimpleClass = new SimpleClass();

//            MutualReferenceTest22 test22 = new AAA();
//            test22._Num = 200;
//            test22._Mutual = null;
//
//            var aaa = new AAA();
//            aaa.Property = new PropertyA();

            var serialized = new JsonSerializer().Serialize(mutualReference);
            Debug.LogError(serialized);
            _SerializedJson = serialized;
        }

        [MenuItem("CustomSerialize/TestDeSerialize")]
        static void TestDeSerialize()
        {
            if (_SerializedJson != null)
            {
                var deserialized = new JsonDeSerializer().DeSerialize<MutualReferenceTest>(_SerializedJson);

                Debug.LogError("deserialized = " + deserialized);
                Debug.LogError("dict count = " + deserialized._dict.Count);
                foreach (var temp in deserialized._dict)
                {
                    Debug.LogError("key = " + temp.Key + " value = " + temp.Value);
                }
                Debug.LogError("vect = " + deserialized._Vector3 + " x = " + deserialized._Vector3.x);
            }
        }

        [MenuItem("CustomSerialize/SimpleTest")]
        static void SimpleTestSerialize()
        {
            if (true)
            {
                Dictionary<string, Type> dictionary = new Dictionary<string, Type>()
                {
                    {"string", typeof(string) },
                    {"int", typeof(int) },
                    {"person", typeof(PersonInfo) },
                    {"person22", typeof(PersonInfo) },
                };
                
//                Dictionary<string, Dictionary<int, GameObject>> dict2222 = new Dictionary<string, Dictionary<int, GameObject>>()
//                {
//                    {"one",new Dictionary<int, GameObject>()
//                    {
//                        {1,new GameObject("obj") }
//                    }},
//                    {"Two",new Dictionary<int, GameObject>()
//                    {
//                        {2,new GameObject("888") }
//                    }}
//                };

                Dictionary<string, GameObject> dict33 = new Dictionary<string, GameObject>()
                {
                    {"Three", new GameObject("Three")}, 
                };
                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/prefab.prefab");
                dict33.Add("prefab", prefab);

                var serialized = new JsonSerializer().Serialize(dict33);
                Debug.LogError(serialized);
                
                var deserialized = new JsonDeSerializer().DeSerialize<Dictionary<string, GameObject>>(serialized);
                Debug.LogError("deserialized = \n" + deserialized);

                return;
            }

            Dictionary<PersonInfo, PersonDetailInfo> personsDict = new Dictionary<PersonInfo, PersonDetailInfo>()
            {
                {new PersonInfo(), new PersonDetailInfo()},
                {new PersonInfo(892374, "QQQ"), new PersonDetailInfo(0, 20, 168f, Color.white, 'Q')},
            };

            Dictionary<string, int> dict = new Dictionary<string, int>()
            {
                {"Luo",1 },
                {"Jia",2 },
                {"Quan",3 },
            };

            Dictionary<string, int> dict0 = new Dictionary<string, int>()
            {
                {"AAA",1 },
                {"$$$",2 },
                {"&&&",3 },
            };

            Dictionary<long, string> dict2 = new Dictionary<long, string>()
            {
                {12324551,"AAA" },
                {62722356,"BBB" },
                {33536112,"CCC" },
            };

            Dictionary<Dictionary<string,int>, string> dict3 = new Dictionary<Dictionary<string, int>, string>()
            {
                {dict, "dict1" },
                {dict0, "dict0" }
            };

            List<Dictionary<string,List<int>>> list = new List<Dictionary<string, List<int>>>();
            list.Add(new Dictionary<string, List<int>>()
            {
                {"luo", new List<int>(){1,2,3} }
            });

            List<Dictionary<string,int>> list2 = new List<Dictionary<string, int>>();
            list2.Add(new Dictionary<string, int>()
            {
                {"AAA",1234 }
            });

            //            var serialized = new JsonSerializer().Serialize(personsDict);
            //            Debug.LogError(serialized);
            //            
            //            var deserialized = new JsonDeSerializer().DeSerialize<Dictionary<PersonInfo, PersonDetailInfo>>(serialized);
            //            Debug.LogError("deserialized = \n" + deserialized);

            List<string> strList = new List<string>()
            {
                "Dictionary[Dictionary[Dictionary[List[HashSet[System.String]], System.Int32], System.Int32], Dictionary[System.Int64, System.Int32]]",
                "Dictionary[System.String, Dictionary[Dictionary[System.String, System.Int32], System.Int32]]",
                "Dictionary[List[System.Int32], Dictionary[System.String, System.Int32]]",
                "Dictionary[List[HashSet[System.Int32]], System.Int32]",
                "Dictionary[System.Int32, List[HashSet[System.Int32]]]",
                "Dictionary[Dictionary[System.String, System.Int32],Dictionary[System.String, System.Int32]]",
                "List[System.Int32]",
                "List[HashSet[System.Int32]]",
            };

            string substr = "Dictionary[Dictionary[List[HashSet[System.String]], System.Int32], System.Int32], Dictionary[System.Int64, System.Int32]";
            string substr2 = "System.Int32, List[HashSet[System.Int32]], System.String";
//            foreach (var str in strList)
//            {
//                Type type = GetCustomType2(str);
//                Debug.LogError("type = " + type);
//            }

            List<string> simpleStr = new List<string>()
            {
//                "List[HashSet[System.Int32]]",
                "Dictionary[System.Int32, List[HashSet[System.Int32]]]",
//                "Dictionary[System.Int32, System.String]",
//                "Dictionary[List[System.Int32], System.String]",
//                "List[System.Int32]",
                "BBB[Dictionary[System.Int32,List[HashSet[System.String]]], List[System.String], HashSet[Dictionary[System.Int64, List[System.Int32]]]]",
            };

            foreach (var str in simpleStr)
            {
                //Debug.LogError("str = " + str);
                //Type type = RecursionGetCustomType1(str);
                Type type = RecursionGetCustomType2(str);
                Debug.LogError("type = " + type);
            }

        }

        public class BBB<T, K, V>
        {
        }

        private static Type RecursionGetCustomType1(string val)
        {
            val = val.Trim();
            if (String2TypeMap.ContainsKey(val))
            {
                return String2TypeMap[val];
            }

            Type type = Type.GetType(val);
            if (type != null)
            {
                return type;
            }

            string currentTypeStr = null;
            if (val.Contains("["))
            {
                currentTypeStr = val.Substring(0, val.IndexOf('['));
                var currentType = RecursionGetCustomType1(currentTypeStr);
                var argumentNums = currentType.IsGenericType ? currentType.GetGenericArguments().Length : 0;
                if (argumentNums > 0)
                {
                    int firstIndexOfLeftSquare = val.IndexOf('[');
                    var subStr = val.Substring(firstIndexOfLeftSquare + 1, val.LastIndexOf(']') - firstIndexOfLeftSquare - 1);
                    string[] nextStr = new string[argumentNums];
                    if (argumentNums > 1)
                    {
                        int leftSquareCount = 0;
                        int rightSuqareCount = 0;
                        int index = 0;
                        int finishIndex = 0;
                        int length = subStr.Length;
                        for (int i = 0; i < length; i++)
                        {
                            char c = subStr[i];
                            if (c == ' ')
                            {
                                continue;
                            }
                            else if (c == '[')
                            {
                                leftSquareCount++;
                            }
                            else if (c == ']')
                            {
                                rightSuqareCount++;
                                if (leftSquareCount == rightSuqareCount)
                                {
                                    nextStr[index] = subStr.Substring(finishIndex, i - finishIndex + 1);
                                    index++;
                                    finishIndex = i + 1;
                                }
                            }
                            else if (c == ',')
                            {
                                if (finishIndex == i)
                                {
                                    finishIndex = i + 1;
                                    continue;
                                }
                                if (leftSquareCount == rightSuqareCount)
                                {
                                    nextStr[index] = subStr.Substring(finishIndex, i - finishIndex);
                                    index++;
                                    finishIndex = i + 1;
                                }
                            }

                            if (i == length - 1 && finishIndex < i)
                            {
                                nextStr[index] = subStr.Substring(finishIndex, i - finishIndex + 1);
                            }
                        }
                    }
                    else
                    {
                        nextStr[0] = subStr;
                    }

                    //var nextStr = subStr.Split(',');
                    Type[] arguments = new Type[argumentNums];
                    for (int i = 0; i < argumentNums; i++)
                    {
                        //Debug.LogError("nextStr[i] = " + nextStr[i]);
                        arguments[i] = RecursionGetCustomType1(nextStr[i]);
                    }

                    return currentType.MakeGenericType(arguments);
                }
                else
                {
                    Debug.LogError("argumentNums == 0, type = " + currentType);
                }
            }
            Debug.LogError("this typeString can not parse, typeStr = " + val);
            return null;
        }

        List<string> simpleStr111 = new List<string>()
        {
//                "List[HashSet[System.Int32]]",
            "Dictionary[System.Int32, List[HashSet[System.Int32]]]",
//                "Dictionary[System.Int32, System.String]",
//                "Dictionary[List[System.Int32], System.String]",
//                "List[System.Int32]",
            "BBB[Dictionary[System.Int32,List[HashSet[System.String]]], List[System.String], HashSet[Dictionary[System.Int64, List[System.Int32]]]]",
        };

        private string st = "Dictionary[System.Int32" + "List[HashSet[System.String]]]" + " List[System.String]" +
                            " HashSet[Dictionary[System.Int64" + " List[System.Int32]]]";

        private static Dictionary<string, Type> String2TypeMap = new Dictionary<string, Type>()
        {
            {"Dictionary", typeof(Dictionary<,>) },
            {"List", typeof(List<>) },
            {"HashSet", typeof(HashSet<>) },
            {"Stack", typeof(Stack<>) },
            {"Queue", typeof(Queue<>) },
            {"BBB", typeof(BBB<,,>) },
        };

        private static Type RecursionGetCustomType2(string val)
        {
            val = val.Trim();
            if (String2TypeMap.ContainsKey(val))
            {
                return String2TypeMap[val];
            }
            Type type = Type.GetType(val);
            if (type != null)
            {
                return type;
            }

            string currentTypeStr = null;
            if (val.Contains("["))
            {
                currentTypeStr = val.Substring(0, val.IndexOf('['));
                var currentType = RecursionGetCustomType1(currentTypeStr);
                var argumentNums = currentType.IsGenericType ? currentType.GetGenericArguments().Length : 0;
                if (argumentNums > 0)
                {
                    int firstIndexOfLeftSquare = val.IndexOf('[');
                    var subStr = val.Substring(firstIndexOfLeftSquare + 1, val.LastIndexOf(']') - firstIndexOfLeftSquare - 1);
                    string[] nextStr = new string[argumentNums];


                    Type[] arguments = new Type[argumentNums];
                    for (int i = 0; i < argumentNums; i++)
                    {
                        //Debug.LogError("nextStr[i] = " + nextStr[i]);
                        arguments[i] = RecursionGetCustomType1(nextStr[i]);
                    }

                    return currentType.MakeGenericType(arguments);
                }
                else
                {
                    Debug.LogError("argumentNums == 0, type = " + currentType);
                }
            }
            Debug.LogError("this typeString can not parse, typeStr = " + val);
            return null;
        }

        private static Type GetCustomType2(string val)
        {
            int finishedIndex = 0;
            Stack<int> waitingTypesIndex = new Stack<int>();
            List<Type> typeList = new List<Type>();
            int currentTypeIndex = 0;
            Action<Type> push = (type) =>
            {
                if (currentTypeIndex == typeList.Count)
                {
                    typeList.Add(type);
                }
                else
                {
                    typeList[currentTypeIndex] = type;
                }
                currentTypeIndex++;
            };
            Action<int> pop = (count) => { currentTypeIndex -= count; };

            for (int i = 0; i < val.Length; i++)
            {
                char c = val[i];
                if (c == ' ')
                {
                    continue;
                }
                else if(c == '[')
                {
                    Type type = GetType(val.Substring(finishedIndex, i - finishedIndex));
                    finishedIndex = i + 1;
                    push(type);
                    waitingTypesIndex.Push(currentTypeIndex - 1);
                }
                else if(c == ']')
                {
                    Type type = GetType(val.Substring(finishedIndex, i - finishedIndex));
                    finishedIndex = i + 1;

                    int waitingTypeIndex = waitingTypesIndex.Pop();
                    int argumentLength = currentTypeIndex - waitingTypeIndex - 1;
                    if (type != null)
                    {
                        argumentLength++;
                    }

                    Type[] typeArguments = new Type[argumentLength];
                    for (int j = 0; j < typeArguments.Length - 1; j++)
                    {
                        typeArguments[j] = typeList[j + waitingTypeIndex + 1];
                    }
                    typeArguments[typeArguments.Length - 1] = type ?? typeList[typeArguments.Length + waitingTypeIndex];

                    Type currentType = typeList[waitingTypeIndex];
                    currentType = currentType.MakeGenericType(typeArguments);
                    pop(currentTypeIndex - waitingTypeIndex);
                    push(currentType);
                }
                else if(c == ',')
                {
                    if (i == finishedIndex)
                    {
                        finishedIndex = i + 1;
                        continue;
                    }
                    push(GetType(val.Substring(finishedIndex, i - finishedIndex)));
                    finishedIndex = i + 1;
                }
                
            }
            return typeList[currentTypeIndex - 1];
        }

        private static Type GetType(string typeStr, List<Type> arguments = null, Type waitingType = null)
        {
            if (waitingType != null && arguments != null)
            {
                Debug.LogWarning("waitingType = " + waitingType + " arguments.count = " + arguments.Count);
                return waitingType.MakeGenericType(arguments.ToArray());
            }
            Type type = null;
            typeStr = typeStr.Trim();
            if (String2TypeMap.ContainsKey(typeStr))
            {
                if (arguments == null || arguments.Count <= 0)
                {
                    type = String2TypeMap[typeStr];
                }
                else
                {
                    type = String2TypeMap[typeStr].MakeGenericType(arguments.ToArray());
                }
                return type;
            }
            else
            {
                //Debug.LogError("String2TypeMap do not contains key, key = " + typeStr);
                type = Type.GetType(typeStr);
                if (type != null)
                {
                    return type;
                }
                else
                {
                    //Debug.LogError("this typeStr can not parse to type, typeStr = '" + typeStr + "'");
                    return null;
                }
            }
        }

        public class PersonInfo
        {
            public int ID = 12345678;
            public string name = "luojiaquan";

            public PersonInfo()
            {
            }

            public PersonInfo(int id, string name)
            {
                this.ID = id;
                this.name = name;
            }
        }

        public class PersonDetailInfo
        {
            public int Male = 1;
            public int Age = 18;
            public float Height = 175.2f;
            public Color SkinColor = Color.yellow;
            public char ShortName = 'J';

            public PersonDetailInfo()
            {
            }

            public PersonDetailInfo(int male, int age, float height, Color color, char c)
            {
                Male = male;
                Age = age;
                Height = height;
                SkinColor = color;
                ShortName = c;
            }
        }


        [MenuItem("CustomSerialize/TestCustom")]
        static void TestCustom()
        {
            DemoObject obj = new DemoObject("Luo", new []{ "00-00", "00-022" }, 18, "male", "foshan");
            DemoObject obj2 = new DemoObject("Luo 2", new[] { "33-00", "00-022" }, 19, "male 2", "foshan 2");
            DemoObject obj3 = new DemoObject("Luo 3", new[] { "44-00", "00-044" }, 19, "male 4", "foshan 4");
            DemoObject obj4 = new DemoObject("Luo 4", new []{"10-02", "673-124"}, 20, "male 5", "foshan 5");
            KeyObject key = new KeyObject(1, "obj");
            KeyObject key2 = new KeyObject(2, "obj2");
            KeyObject key3 = new KeyObject(3, "obj3");
            KeyObject key4 = new KeyObject(4, "obj4");
            Dictionary<KeyObject, DemoObject> dict = new Dictionary<KeyObject, DemoObject>();
            dict.Add(key, obj);
            dict.Add(key2, obj2);
            dict.Add(key3, obj3);
            dict.Add(key4, null);
            //object serializer = new JsonSerializer().Serialize(dict);

            //            Dictionary<string, DemoObject11> dict = new Dictionary<string, DemoObject11>();
            //            dict.Add("obj", obj);
            //            dict.Add("obj2", obj2);
            //            dict.Add("obj3", obj3);
            //            List<DemoObject11> list = new List<DemoObject11>();
            //            list.Add(obj);
            //            list.Add(obj2);
            //            list.Add(obj3);
            //object serializer = new JsonSerializer().Serialize(new []{ obj , obj2});
            //object serializer = new JsonSerializer().Serialize(list);

            //            var property = new PropertyTest(175, 100f);
            //            object serializer = new JsonSerializer().Serialize(property);

            MutualReferenceTest mutual01 = new MutualReferenceTest("China");
            MutualReferenceTest22 mutual02 = new MutualReferenceTest22(20);
            mutual01.SetMutaual(mutual02);
            MutualReferenceTest33 mutual03 = new MutualReferenceTest33(0.5f, mutual01);
            mutual02.SetMutual(mutual03);
            var serializer = new JsonSerializer().Serialize(mutual01);
            Debug.LogError(serializer);

//            var obj222 =  Reflections.Clone(mutual01, new []{typeof(Reflections.CloneField)});
//            Debug.LogError(obj222 + " country = " + obj222._Country + " mutual = " + obj222._Mutual);
//            if (obj222._Mutual._Mutual._Mutual.Equals(obj222))
//            {
//                Debug.LogError("same Object");
//            }

            //            MutualReferenceTest mutual04 = new MutualReferenceTest("");
            //
            //
            //Debug.LogError(serializer);
        }

    }

    public enum Animal
    {
        Dog = 1,
        Cat = 2,
        Mouse = 3,
    }

    public interface IAnimal
    {
    }

    public class Dog : IAnimal
    {
        public string Name;
        public Cat Cat;
        public Dog()
        {
        }
        public Dog(string name)
        {
            Name = name;
        }
      
    }
     
    public class Cat : IAnimal
    {
        public string Name;
        public int Age;
        public Dog Dog;
        public Cat()
        {
        }
        public Cat(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }

    public interface IMyInterface
    {

    }

    public class MyImplement : IMyInterface
    {

    }

    public enum SceneNumber
    {
        First = 1,
        Second = 2,
        Third = 3,
    }

    public class SimpleClass
    {
//        public float Value1 = 3f;
//        public int Value2 = 10;
        public Vector3 _Vector3 = new Vector3(1f,1f,1f);
        public Dictionary<string, int> _Dict = new Dictionary<string, int>()
        {
            {"Hello", 1},
            {"World", 2}
        };
        public SceneNumber _SceneNumber = SceneNumber.Second;
        //public IAnimal[] _Animals = new IAnimal[]{new Cat("Cat", 2),new Dog("Dog") };
//        public List<IAnimal> _Animals = new List<IAnimal>()
//        {
//            new Cat("Cat", 2),
//            new Dog("Dog")
//        };
        public int[] _IntNumbers = new[] {1, 2, 3, 4};
        public List<int> _Numbers = new List<int>()
        {
            10,9,8,7,6
        };
//        public List<SceneNumber> _SceneNumList = new List<SceneNumber>()
//        {
//            SceneNumber.First,
//            SceneNumber.Second,
//            SceneNumber.Third
//        };

//        public SimpleClass2 SimpleClass2;

    }

    [Serializable]
    public class SimpleClass2
    {
        public int Age = 18;
        public float Height = 175.2f;
    }

    [SerializeClass(OldName = new []{ "MutualReferenceTest" })]
    public class MutualReferenceTest
    {
        public string _Country;
        public MutualReferenceTest22 _Mutual;
        public IAnimal _FirstAnimal;
        public IAnimal[] _Pets;
        public IMyInterface _Implement;
        public SimpleClass _SimpleClass;
        public Vector3 _Vector3 = new Vector3(2f,2f,2f);
        public Dictionary<string, int> _dict = new Dictionary<string, int>()
        {
            {"luo", 1},
            {"Jia", 2},
            {"quan", 3}
        };

        public MutualReferenceTest()
        {

        }

        public MutualReferenceTest(string country, MutualReferenceTest22 mutual = null)
        {
            _Country = country;
            _Mutual = mutual;
        }

        public void SetMutaual(MutualReferenceTest22 mutual)
        {
            _Mutual = mutual;
        }
    }

    public class AAA : MutualReferenceTest22
    {
        public IProperty Property { get; set; }
    }

    public interface IProperty
    {
    }

    public class PropertyA : IProperty
    {
        public string name = "PropertyA";
    }

    public class PropertyB : IProperty
    {
        public string name = "PropertyB";
    }

    public class MutualReferenceTest22
    {
        [Reflections.CloneField]
        public int _Num;
        [Reflections.CloneField]
        public MutualReferenceTest33 _Mutual;

        public MutualReferenceTest22()
        {

        }

        public MutualReferenceTest22(int num, MutualReferenceTest33 mutual = null)
        {
            _Num = num;
            _Mutual = mutual;
        }

        public void SetMutual(MutualReferenceTest33 mutual)
        {
            _Mutual = mutual;
        }
    }

    public class MutualReferenceTest33
    {
        [Reflections.CloneField]
        public float _FValue;
        [Reflections.CloneField]
        public MutualReferenceTest _Mutual;

        public MutualReferenceTest33()
        {

        }

        public MutualReferenceTest33(float val, MutualReferenceTest mutual)
        {
            _FValue = val;
            _Mutual = mutual;
        }
    }

}
