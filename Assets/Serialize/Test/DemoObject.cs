﻿using SerializeExtend;
using UnityEngine;

namespace SerializeExtend
{
    public class DemoObject
    {
        public string _Name;
        [CustomSerializeField]
        private string[] _BirthDay;
        [CustomSerializeField]
        private int _Age;
        private string _Sex;
        private string _Belongs;
        private Vector3 _Vector4 = new Vector3(1, 1, 1);

        public DemoObject(string name, string[] birthDay, int age, string sex, string belongs)
        {
            _Name = name;
            _BirthDay = birthDay;
            _Age = age;
            _Sex = sex;
            _Belongs = belongs;
        }

        public override string ToString()
        {
            return "Name = " + _Name + " _BirthDay = " + _BirthDay + " _Age = " + _Age + " _Sex = " + _Sex + " _Belongs = " + _Belongs;
        }
    }

    public class PropertyTest
    {
        public int Height { get; set; }

        private float HandLenght { get; set; }

        public PropertyTest(int height, float handLenght)
        {
            Height = height;
            HandLenght = handLenght;
        }
    }

    public class KeyObject
    {
        public int _ID;
        public string _Desc;

        public KeyObject(int id, string desc)
        {
            _ID = id;
            _Desc = desc;
        }
    }

}

