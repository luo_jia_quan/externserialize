﻿
using System.IO;
using System.Xml.Serialization;
using UnityEditor;


public class TestXmlSerialize
{
    [MenuItem("XML/Test")]
    public static void TestXml()
    {
        Hero knight = new Hero();
        knight.name = "Knight of Solamnia";
        knight.isBoss = true;
        knight.hitPoints = 100;
        knight.baseDamage = 50f;

        Serialize(knight, "Assets/hero.xml");
    }

    private static void Serialize(object item, string path)
    {
        XmlSerializer serializer = new XmlSerializer(item.GetType());
        StreamWriter writer = new StreamWriter(path);
        serializer.Serialize(writer.BaseStream, item);
        writer.Close();
    }

}

public class Hero
{
    [XmlElement("n")]
    public string name;

    [XmlAttribute("boss")]
    public bool isBoss;

    public int hitPoints;

    public float baseDamage;
}