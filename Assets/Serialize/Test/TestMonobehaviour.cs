﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class TestMonobehaviour : MonoBehaviour
{
    public GameObject gameObject;



    void OnEnable()
    {
        if (gameObject != null)
        {
            var prefab = PrefabUtility.GetPrefabInstanceHandle(gameObject);
            Object obj = PrefabUtility.GetCorrespondingObjectFromOriginalSource(gameObject);
            string path = AssetDatabase.GetAssetPath(obj);

            Debug.LogError("is prefab = " + PrefabUtility.IsPartOfAnyPrefab(gameObject) + " path = " + path);
            Debug.LogError("scene path = " + gameObject.transform.GetFullHierarchyPath());

            string scenePath = gameObject.transform.GetFullHierarchyPath();

            Debug.LogError("scenePath = " + scenePath + "  find obj = " + GameObjectFindUtils.Find(scenePath));
        }
       
    }

}
