﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace SerializeExtend
{

    public class Reflections
    {
        [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
        public class CloneField : Attribute
        {
        }
        [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
        public class NoCloneField : Attribute
        {
        }
        [AttributeUsage(AttributeTargets.Class)]
        public class ClonePublicField : Attribute
        {
        }

        private class CloneContext
        {
            public struct GenericType
            {
                private Type _Type;
                private Type[] _TypeArguments;
                private int _HashCode;

                public GenericType(Type type, Type[] typeArguments)
                {
                    _Type = type;
                    _TypeArguments = typeArguments;

                    string hashCode = "";
                    hashCode += _Type;

                    if (typeArguments != null && typeArguments.Length != 0)
                    {
                        foreach (var typeArgument in typeArguments)
                        {
                            hashCode += typeArgument;
                        }
                    }

                    _HashCode = hashCode.GetHashCode();
                }

                public override bool Equals(object obj)
                {
                    if (obj == null)
                    {
                        return false;
                    }

                    if (!(obj is GenericType))
                    {
                        return false;
                    }

                    var genericType = (GenericType)obj;


                    if (genericType._Type != _Type)
                    {
                        return false;
                    }

                    if ((_TypeArguments == null && genericType._TypeArguments != null) ||
                        (_TypeArguments != null && genericType._TypeArguments == null))
                    {
                        return false;
                    }

                    if (_TypeArguments == genericType._TypeArguments)
                    {
                        return true;
                    }

                    if (_TypeArguments.Length != genericType._TypeArguments.Length)
                    {
                        return false;
                    }

                    for (int i = 0; i < _TypeArguments.Length; i++)
                    {
                        if (_TypeArguments[i] != genericType._TypeArguments[i])
                        {
                            return false;
                        }
                    }

                    return true;
                }

                public override int GetHashCode()
                {
                    return _HashCode;
                }
            }

            private Dictionary<object, object> _ClonedObj = new Dictionary<object, object>();
            private Dictionary<Type, Dictionary<string, IPropertyOrField>> _PropertiesOrFieldsDict = new Dictionary<Type, Dictionary<string, IPropertyOrField>>();
            private Dictionary<GenericType, Type> _GenericTypeDict = new Dictionary<GenericType, Type>();
            private Dictionary<Type, Dictionary<string, MethodInfo>> _MethodInfos = new Dictionary<Type, Dictionary<string, MethodInfo>>();

            public void AddCloned(object src, object dst)
            {
                _ClonedObj.Add(src, dst);
            }

            public bool HasCloned(object src)
            {
                return _ClonedObj.ContainsKey(src);
            }

            public object GetCloned(object src)
            {
                return _ClonedObj[src];
            }

            public Type MakeOrGetGenericType(Type type, Type[] typeArguments)
            {
                var genericType = new GenericType(type, typeArguments);
                if (_GenericTypeDict.ContainsKey(genericType))
                {
                    return _GenericTypeDict[genericType];
                }

                var makeType = type.MakeGenericType(typeArguments);
                _GenericTypeDict.Add(genericType, makeType);
                return makeType;
            }

            public Dictionary<string, IPropertyOrField> GetPropertiesAndFields(Type type, Type[] attributes)
            {
                if (_PropertiesOrFieldsDict.ContainsKey(type))
                {
                    return _PropertiesOrFieldsDict[type];
                }

                var propertiesOrFields = new Dictionary<string, IPropertyOrField>();

                Action<IPropertyOrField> add = field =>
                {
                    if (field.GetCustomAttribute<NoCloneField>() != null)
                    {
                        return;
                    }

                    if (!propertiesOrFields.ContainsKey(field.Name))
                    {
                        propertiesOrFields.Add(field.Name, field);
                    }
                };

                foreach (var attribute in attributes)
                {
                    var pofs = Reflections.GetPropertiesAndFields(type, attribute);
                    foreach (var propertyOrField in pofs)
                    {
                        add(propertyOrField);
                    }
                }

                if (type.GetCustomAttribute(typeof(ClonePublicField)) != null)
                {
                    var pofs = Reflections.GetPropertiesAndFields(type, null, BindingFlags.Instance | BindingFlags.Public);
                    foreach (var propertyOrField in pofs)
                    {
                        add(propertyOrField);
                    }
                }

                _PropertiesOrFieldsDict.Add(type, propertiesOrFields);
                return propertiesOrFields;
            }

            public MethodInfo GetMethod(Type type, string method, BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
            {
                Dictionary<string, MethodInfo> name2Method = null;
                if (_MethodInfos.ContainsKey(type))
                {
                    name2Method = _MethodInfos[type];
                }
                else
                {
                    name2Method = new Dictionary<string, MethodInfo>();
                    _MethodInfos.Add(type, name2Method);
                }

                if (name2Method.ContainsKey(method))
                {
                    return name2Method[method];
                }


                var methodInfo = type.GetMethod(method, flags);
                name2Method.Add(method, methodInfo);

                return methodInfo;
            }
        }

        public const int TYPE_CLASS = 1;
        public const int TYPE_ATTRIBUTE = 2;

        private const BindingFlags DEFAULT_BINDING_FLAGS = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;

        private static readonly ITypeHolder DEFAULT_TYPE_HOLDER = new TypeHolder();
        private static readonly Type _DICT_TYPE = typeof(Dictionary<,>);
        private static readonly Type _LIST_TYPE = typeof(List<>);
        private static readonly Type _SET_TYPE = typeof(HashSet<>);
        private static readonly Type _STRING_TYPE = typeof(String);

        public static object Clone(object obj, params Type[] attributes)
        {
            return Clone(new CloneContext(), obj, attributes);
        }

        public static T Clone<T>(T t, params Type[] attributes) where T : class
        {
            if (t == null)
            {
                return t;
            }

            var obj = Clone((object)t, attributes);

            if (obj == null)
            {
                return null;
            }

            return (T)obj;
        }

        private static object Clone(CloneContext ctx, object src, params Type[] attributes)
        {
            if (src == null)
            {
                return null;
            }

            if (ctx.HasCloned(src))
            {
                return ctx.GetCloned(src);
            }

            Type type = src.GetType();

            if (type.IsArray)
            {
                Array oldArray = (Array)src;
                var dst = Array.CreateInstance(type.GetElementType(), oldArray.Length);
                ctx.AddCloned(src, dst);
                for (int i = 0; i < oldArray.Length; i++)
                {
                    dst.SetValue(Clone(ctx, oldArray.GetValue(i), attributes), i);
                }
                return dst;
            }

            if (type.IsGenericType)
            {
                if (type.GetGenericTypeDefinition() == _DICT_TYPE)
                {
                    var dictType = ctx.MakeOrGetGenericType(_DICT_TYPE, type.GenericTypeArguments);
                    var dst = (System.Collections.IDictionary)Activator.CreateInstance(dictType);
                    ctx.AddCloned(src, dst);
                    var oldDict = (System.Collections.IDictionary)src;
                    foreach (var key in oldDict.Keys)
                    {
                        dst.Add(key, Clone(ctx, oldDict[key], attributes));
                    }

                    return dst;
                }
                if (type.GetGenericTypeDefinition() == _LIST_TYPE)
                {
                    var listType = ctx.MakeOrGetGenericType(_LIST_TYPE, type.GenericTypeArguments);
                    var dst = (System.Collections.IList)Activator.CreateInstance(listType);
                    ctx.AddCloned(src, dst);
                    var oldlist = (System.Collections.IList)src;
                    foreach (var item in oldlist)
                    {
                        dst.Add(Clone(ctx, item, attributes));
                    }

                    return dst;
                }
                if (type.GetGenericTypeDefinition() == _SET_TYPE)
                {
                    var setType = ctx.MakeOrGetGenericType(_SET_TYPE, type.GenericTypeArguments);
                    var dst = Activator.CreateInstance(setType);
                    ctx.AddCloned(src, dst);

                    var addMethod = ctx.GetMethod(setType, "Add");
                    var oldSet = (System.Collections.IEnumerable)src;
                    foreach (var item in oldSet)
                    {
                        addMethod.Invoke(dst, new[] { Clone(ctx, item, attributes) });
                    }
                    return dst;
                }
            }

            if (type.IsEnum)
            {
                var dst = Enum.ToObject(type, src);
                ctx.AddCloned(src, dst);
                return dst;
            }

            if (type.IsPrimitive || type == _STRING_TYPE)
            {
                return src;
            }

            {
                Dictionary<string, IPropertyOrField> propertiesOrFields = ctx.GetPropertiesAndFields(type, attributes);

                if (propertiesOrFields.Count == 0)
                {
                    return src;
                }

                var dst = Activator.CreateInstance(type);
                ctx.AddCloned(src, dst);

                foreach (var propertiesOrField in propertiesOrFields.Values)
                {
                    propertiesOrField.SetValue(dst, Clone(ctx, propertiesOrField.GetValue(src), attributes));
                }

                return dst;
            }
        }

        /// <summary>
        /// 从指定的typeHolder提供的所有Type中找到继承了baseOrInterface类型的所有类型。
        /// </summary>
        /// <param name="typeHolder"></param>
        /// <param name="baseOrInterface"></param>
        /// <param name="allowAbstract"></param>
        /// <returns></returns>
        public static List<Type> GetTypes(ITypeHolder typeHolder, Type baseOrInterface, bool allowAbstract = false)
        {
            var types = typeHolder.GetTypes();

            List<Type> validTypes = new List<Type>();
            foreach (var type in types)
            {
                if (baseOrInterface.IsAssignableFrom(type) && (allowAbstract || !type.IsAbstract))
                {
                    validTypes.Add(type);
                }
            }

            return validTypes;
        }

        public static List<Type> GetTypes(Type baseOrInterface, bool allowAbstract = false)
        {
            return GetTypes(DEFAULT_TYPE_HOLDER, baseOrInterface, allowAbstract);
        }

        /// <summary>
        /// 从指定typeHolder中获得所有带有attributeTypes其中某个Attribute的Type
        /// </summary>
        /// <param name="typeHolder"></param>
        /// <param name="attributeTypes"></param>
        /// <param name="typeType"></param>
        /// <returns></returns>
        public static List<Type> GetTypesWithAttributes(ITypeHolder typeHolder, IEnumerable<Type> attributeTypes, int typeType = TYPE_CLASS)
        {
            var types = typeHolder.GetTypes();
            List<Type> list = new List<Type>();
            Type attribute = typeof(Attribute);
            foreach (var type in types)
            {
                if ((typeType == TYPE_CLASS && attribute.IsAssignableFrom(type)) || (typeType == TYPE_ATTRIBUTE && !attribute.IsAssignableFrom(type)))
                {
                    continue;
                }

                bool valid = false;
                foreach (var attributeType in attributeTypes)
                {
                    if (type.GetCustomAttribute(attributeType) != null)
                    {
                        valid = true;
                        break;
                    }
                }

                if (valid)
                {
                    list.Add(type);
                }
            }
            return list;
        }

        public static List<Type> GetTypesWithAttributes(ITypeHolder typeHolder, Type attributeType, int typeType = TYPE_CLASS)
        {
            return GetTypesWithAttributes(typeHolder, new List<Type> { attributeType }, typeType);
        }

        /// <summary>
        /// 获取指定obj上带有Attribute为A的所有FieldInfo
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="bindingFlags"></param>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static List<FieldInfo> GetFields<A>(object obj, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS) where A : Attribute
        {
            return GetFields<A>(obj.GetType(), bindingFlags);
        }

        public static List<FieldInfo> GetFields(object obj, Type attribute, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS)
        {
            return GetFields(obj.GetType(), attribute, bindingFlags);
        }

        /// <summary>
        /// 获取指定type上带有Attribute为A的所有FieldInfo
        /// </summary>
        /// <param name="type"></param>
        /// <param name="bindingFlags"></param>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static List<FieldInfo> GetFields<A>(Type type, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS) where A : Attribute
        {
            return GetFields(type, typeof(A), bindingFlags);
        }

        public static List<FieldInfo> GetFields(Type type, Type attribute = null, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS)
        {
            var fields = type.GetFields(bindingFlags);

            List<FieldInfo> fieldInfos = new List<FieldInfo>();

            for (int i = 0; i < fields.Length; i++)
            {
                if (attribute == null || fields[i].GetCustomAttribute(attribute) != null)
                {
                    fieldInfos.Add(fields[i]);
                }
            }

            return fieldInfos;
        }

        /// <summary>
        /// 获取指定type上带有Attribute为A的所有PropertyInfo
        /// </summary>
        /// <param name="type"></param>
        /// <param name="bindingFlags"></param>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static List<PropertyInfo> GetProperties<A>(Type type, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS) where A : Attribute
        {
            return GetProperties(type, typeof(A), bindingFlags);
        }

        /// <summary>
        /// 获取指定type上带有Attribute为A的所有PropertyInfo
        /// </summary>
        /// <param name="type"></param>
        /// <param name="attribute"></param>
        /// <param name="bindingFlags"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetProperties(Type type, Type attribute = null, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS)
        {
            var fields = type.GetProperties(bindingFlags);

            List<PropertyInfo> fieldInfos = new List<PropertyInfo>();

            for (int i = 0; i < fields.Length; i++)
            {
                if (attribute == null || fields[i].GetCustomAttribute(attribute) != null)
                {
                    fieldInfos.Add(fields[i]);
                }
            }

            return fieldInfos;
        }

        /// <summary>
        /// 获取指定obj上带有Attribute为A的所有PropertyInfo
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="bindingFlags"></param>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static List<PropertyInfo> GetProperties<A>(object obj, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS) where A : Attribute
        {
            return GetProperties<A>(obj.GetType(), bindingFlags);
        }

        /// <summary>
        /// 获取指定obj上带有Attribute为A的所有PropertyInfo
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="attribute"></param>
        /// <param name="bindingFlags"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetProperties(object obj, Type attribute, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS)
        {
            return GetProperties(obj.GetType(), attribute, bindingFlags);
        }

        /// <summary>
        /// 获取指定obj上带有Attribute为A的所有PropertyInfo和FieldInfo
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="bindingFlags"></param>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static List<IPropertyOrField> GetPropertiesAndFields<A>(object obj, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS) where A : Attribute
        {
            return GetPropertiesAndFields<A>(obj.GetType(), bindingFlags);
        }

        public static List<IPropertyOrField> GetPropertiesAndFields(object obj, Type attribute, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS)
        {
            return GetPropertiesAndFields(obj.GetType(), attribute, bindingFlags);
        }

        /// <summary>
        /// 获取指定type上带有Attribute为A的所有PropertyInfo和FieldInfo
        /// </summary>
        /// <param name="type"></param>
        /// <param name="bindingFlags"></param>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static List<IPropertyOrField> GetPropertiesAndFields<A>(Type type, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS) where A : Attribute
        {
            return GetPropertiesAndFields(type, typeof(A), bindingFlags);
        }

        public static List<IPropertyOrField> GetPropertiesAndFields(Type type, Type attribute = null, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS)
        {
            List<IPropertyOrField> list = new List<IPropertyOrField>();

            foreach (var prop in GetProperties(type, attribute, bindingFlags))
            {
                list.Add(new Property(prop));
            }

            foreach (var field in GetFields(type, attribute, bindingFlags))
            {
                list.Add(new Field(field));
            }

            return list;
        }

        /// <summary>
        /// 查找指定type上带有Attribute为T的方法
        /// </summary>
        /// <param name="type"></param>
        /// <para> name="bindingFlags"</para>>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static List<MethodInfo> GetMethods<T>(Type type, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS) where T : Attribute
        {
            return GetMethods(type, typeof(T), bindingFlags);
        }

        /// <summary>
        /// 查找指定type上带有Attribute为attribute的方法
        /// </summary>
        /// <param name="type"></param>
        /// <param name="attribute"></param>
        /// <param name="bindingFlags"></param>
        /// <returns></returns>
        public static List<MethodInfo> GetMethods(Type type, Type attribute, BindingFlags bindingFlags = DEFAULT_BINDING_FLAGS)
        {
            List<MethodInfo> methodInfos = new List<MethodInfo>();

            var methods = type.GetMethods(bindingFlags);
            foreach (var method in methods)
            {
                if (method.GetCustomAttribute(attribute) != null)
                {
                    methodInfos.Add(method);
                }
            }

            return methodInfos;
        }
    }

    public interface IPropertyOrField
    {
        string Name { get; }

        void SetValue(object obj, object val);

        A GetCustomAttribute<A>(bool inherit = false) where A : Attribute;

        IEnumerable<A> GetCustomAttributes<A>(bool inherit = false) where A : Attribute;

        Attribute GetCustomAttribute(Type attributeType, bool inherit = false);

        object[] GetCustomAttributes(Type attributeType, bool inherit = false);

        Type GetFieldOrPropertyType();
        object GetValue(object o);
    }

    internal class Property : IPropertyOrField
    {
        private PropertyInfo _PropertyInfo;

        public Property(PropertyInfo prop)
        {
            _PropertyInfo = prop;
        }

        public string Name => _PropertyInfo.Name;

        public void SetValue(object obj, object val)
        {
            _PropertyInfo.SetValue(obj, val);
        }

        public A GetCustomAttribute<A>(bool inherit = false) where A : Attribute
        {
            return _PropertyInfo.GetCustomAttribute<A>(inherit);
        }

        public IEnumerable<A> GetCustomAttributes<A>(bool inherit = false) where A : Attribute
        {
            return _PropertyInfo.GetCustomAttributes<A>(inherit);
        }

        public Attribute GetCustomAttribute(Type attributeType, bool inherit = false)
        {
            return _PropertyInfo.GetCustomAttribute(attributeType, inherit);
        }

        public object[] GetCustomAttributes(Type attributeType, bool inherit = false)
        {
            return _PropertyInfo.GetCustomAttributes(attributeType, inherit);
        }

        public Type GetFieldOrPropertyType()
        {
            return _PropertyInfo.PropertyType;
        }

        public object GetValue(object o)
        {
            return _PropertyInfo.GetValue(o);
        }
    }

    internal class Field : IPropertyOrField
    {
        private FieldInfo _FieldInfo;

        public Field(FieldInfo field)
        {
            _FieldInfo = field;
        }

        public string Name => _FieldInfo.Name;

        public void SetValue(object obj, object val)
        {
            _FieldInfo.SetValue(obj, val);
        }

        public A GetCustomAttribute<A>(bool inherit = false) where A : Attribute
        {
            return _FieldInfo.GetCustomAttribute<A>(inherit);
        }

        public IEnumerable<A> GetCustomAttributes<A>(bool inherit = false) where A : Attribute
        {
            return _FieldInfo.GetCustomAttributes<A>(inherit);
        }

        public Attribute GetCustomAttribute(Type attributeType, bool inherit = false)
        {
            return _FieldInfo.GetCustomAttribute(attributeType, inherit);
        }

        public object[] GetCustomAttributes(Type attributeType, bool inherit = false)
        {
            return _FieldInfo.GetCustomAttributes(attributeType, inherit);
        }

        public Type GetFieldOrPropertyType()
        {
            return _FieldInfo.FieldType;
        }

        public object GetValue(object o)
        {
            return _FieldInfo.GetValue(o);
        }
    }

    public interface ITypeHolder
    {
        IEnumerable<Type> GetTypes();
    }

    public class TypeHolderCollection : ITypeHolder
    {
        private IEnumerable<ITypeHolder> _TypeHolders;

        public TypeHolderCollection(IEnumerable<ITypeHolder> typeHolders)
        {
            _TypeHolders = typeHolders;
        }

        public IEnumerable<Type> GetTypes()
        {
            var types = new HashSet<Type>();
            foreach (var typeHolder in _TypeHolders)
            {
                var ts = typeHolder.GetTypes();
                if (ts == null)
                {
                    continue;
                }

                foreach (var t in ts)
                {
                    types.Add(t);
                }
            }

            return types;
        }
    }

    public class TypeHolder : ITypeHolder
    {
        public IEnumerable<Type> GetTypes()
        {
            return Assembly.GetExecutingAssembly().GetTypes();
        }
    }
}