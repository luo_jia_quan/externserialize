﻿namespace SerializeExtend
{
    public interface ISerializer
    {
        object Serialize(object obj);
    }
}
