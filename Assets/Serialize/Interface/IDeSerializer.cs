﻿using System;

namespace SerializeExtend
{
    public interface IDeSerializer
    {
        object DeSerialize(object obj, Type type);
    }
}

