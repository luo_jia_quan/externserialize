﻿using System;
using NUnit.Framework;
using SerializeExtend;
using UnityEngine;

namespace Tests
{
    public class PrimitiveObject
    {
        public int Age;
        public float Height;
        public char Char;
        public long ID;
        public double Val;
        public long Money;
        public bool Single;

        public PrimitiveObject(int age, float height, char c, long id, double val, long money, bool single)
        {
            Age = age;
            Height = height;
            Char = c;
            ID = id;
            Val = val;
            Money = money;
            Single = single;
        }
    }

    public class TestPrimitiveSerialize
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestPrimitiveSerializeSimplePasses()
        {
            PrimitiveObject obj = new PrimitiveObject(28, 175f, 'L', 24081883, 182.3727d,2000000,true);
            var serialized = new JsonSerializer().Serialize(obj);
            string result = "{\"Datas\":{\"$Ref\":\"1\"}, \"$Type\":{\"1\":\"Tests.PrimitiveObject\"}, \"$Ref\":{\"1\":{\"$Type\":\"1\", \"Age\":\"28\", \"Height\":\"175\", \"Char\":\"L\", \"ID\":\"24081883\", \"Val\":\"182.3727\", \"Money\":\"2000000\", \"Single\":\"True\"}}}";
            Debug.Assert(result == Convert.ToString(serialized), " TestPrimitiveSerializeSimplePasses Failed!  serialized = " + serialized);
        }

        [Test]
        public void TestIntJsonSerialize()
        {
            int a = 100;
            var serialized = new JsonSerializer().Serialize(a);
            string reslut = "{\"Datas\":\"100\"}";
            Debug.Assert(reslut == Convert.ToString(serialized), "TestIntJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestUIntJsonSerialize()
        {
            uint a = 100;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"100\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestUIntJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestFloatJsonSerialize()
        {
            float a = 99.99f;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"99.99\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestFloatJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestDoubleJsonSerialize()
        {
            double a = 99.99999d;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"99.99999\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestDoubleJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestLongJsonSerialize()
        {
            long a = 1011212;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"1011212\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestLongJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestULongJsonSerialize()
        {
            ulong a = 1011212;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"1011212\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestULongJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestShortJsonSerialize()
        {
            short a = 8988;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"8988\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestShortJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestUShortJsonSerialize()
        {
            ushort a = 999;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"999\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestUShortJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestByteJsonSerialize()
        {
            byte a = 10;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"10\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestByteJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestSByteJsonSerialize()
        {
            sbyte a = 10;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"10\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestSByteJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestBoolJsonSerialize()
        {
            bool a = true;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"True\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestBoolJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestCharJsonSerialize()
        {
            char a = 'a';
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"a\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestCharJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestDecimalJsonSerialize()
        {
            decimal a = 10;
            var serialized = new JsonSerializer().Serialize(a);
            string result = "{\"Datas\":\"10\"}";
            Debug.Assert(result == Convert.ToString(serialized), "TestDecimalJsonSerialize Failed, Result = " + serialized);
        }

        [Test]
        public void TestVector2JsonSerialize()
        {
            Vector2 vector2 = Vector2.one;
            var serialized = new JsonSerializer().Serialize(vector2);
            string result = "{\"Datas\":{\"x\":1,\"y\":1}}";
            Debug.Assert(result == Convert.ToString(serialized), "TestVector2JsonSerialize Failed, serialized = " + serialized);
        }

        [Test]
        public void TestVector3JsonSerialize()
        {
            Vector3 vector3 = Vector3.one;
            var serialized = new JsonSerializer().Serialize(vector3);
            string result = "{\"Datas\":{\"x\":1,\"y\":1,\"z\":1}}";
            Debug.Assert(result == Convert.ToString(serialized), "TestVector3JsonSerialize Failed, serialized = " + serialized);
        }

        [Test]
        public void TestQuaternionSerialize()
        {
            Quaternion quaternion = Quaternion.identity;
            var serialized = new JsonSerializer().Serialize(quaternion);
            string result = "{\"Datas\":{\"x\":0,\"y\":0,\"z\":0,\"w\":1}}";
            Debug.Assert(result == Convert.ToString(serialized), "TestQuaternionSerialize Failed, serialized = " + serialized);
        }

        [Test]
        public void TestColorSerialize()
        {
            Color color = Color.green;
            var serialized = new JsonSerializer().Serialize(color);
            string result = "{\"Datas\":{\"r\":0,\"g\":1,\"b\":0,\"a\":1}}";
            Debug.Assert(result == Convert.ToString(serialized), "TestColorSerialize Failed, serialized = " + serialized);
        }

        [Test]
        public void TestRectSerialize()
        {
            Rect rect = new Rect(1,1,1,1);
            var serialized = new JsonSerializer().Serialize(rect);
            string result = "{\"Datas\":{\"x\":1,\"y\":1,\"width\":1,\"height\":1}}";
            Debug.Assert(result == Convert.ToString(serialized), "TestRectSerialize Failed, serialized = " + serialized);
        }

    }

}
