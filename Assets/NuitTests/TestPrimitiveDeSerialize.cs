﻿using NUnit.Framework;
using SerializeExtend;
using UnityEngine;

namespace Tests
{
    public class TestPrimitiveDeSerialize
    {

        [Test]
        public void TestIntJsonDeSerialize()
        {
            int val = 100;
            string json = "{\"Datas\":\"100\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<int>(json);
            Debug.Assert(deSerialize == val, " TestIntJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestUIntJsonDeSerialize()
        {
            uint val = 100;
            string json = "{\"Datas\":\"100\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<uint>(json);
            Debug.Assert(deSerialize == val, "TestUIntJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestFloatJsonDeSerialize()
        {
            float val = 99.99f;
            string json = "{\"Datas\":\"99.99\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<float>(json);
            Debug.Assert(deSerialize == val, "TestFloatJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestDoubleJsonDeSerialize()
        {
            double val = 99.99999d;
            string json = "{\"Datas\":\"99.99999\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<double>(json);
            Debug.Assert(deSerialize == val, "TestDoubleJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestLongJsonDeSerialize()
        {
            long val = 1011212;
            string json = "{\"Datas\":\"1011212\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<long>(json);
            Debug.Assert(deSerialize == val, "TestLongJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestULongJsonDeSerialize()
        {
            ulong val = 1011212;
            string json = "{\"Datas\":\"1011212\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<ulong>(json);
            Debug.Assert(deSerialize == val, "TestULongJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestShortJsonDeSerialize()
        {
            short val = 8988;
            string json = "{\"Datas\":\"8988\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<short>(json);
            Debug.Assert(deSerialize == val, "TestShortJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestUShortJsonDeSerialize()
        {
            ushort val = 999;
            string json = "{\"Datas\":\"999\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<ushort>(json);
            Debug.Assert(deSerialize == val, "TestUShortJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestByteJsonDeSerialize()
        {
            byte val = 10;
            string json = "{\"Datas\":\"10\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<byte>(json);
            Debug.Assert(deSerialize == val, "TestByteJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestSByteJsonDeSerialize()
        {
            sbyte val = 10;
            string json = "{\"Datas\":\"10\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<sbyte>(json);
            Debug.Assert(deSerialize == val, "TestSByteJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestBoolJsonDeSerialize()
        {
            bool val = true;
            string json = "{\"Datas\":\"True\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<bool>(json);
            Debug.Assert(deSerialize == val, "TestBoolJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestCharJsonDeSerialize()
        {
            char val = 'a';
            string json = "{\"Datas\":\"a\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<char>(json);
            Debug.Assert(deSerialize == val, "TestCharJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestDecimalJsonDeSerialize()
        {
            decimal val = 10;
            string json = "{\"Datas\":\"10\"}";
            var deSerialize = new JsonDeSerializer().DeSerialize<decimal>(json);
            Debug.Assert(deSerialize == val, "TestDecimalJsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestVector2JsonDeSerialize()
        {
            Vector2 val = Vector2.one;
            string json = "{\"Datas\":{\"x\":1,\"y\":1}}";
            var deSerialize = new JsonDeSerializer().DeSerialize<Vector2>(json);
            Debug.Assert(deSerialize == val, "TestVector2JsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestVector3JsonDeSerialize()
        {
            Vector3 val = Vector3.one;
            string json = "{\"Datas\":{\"x\":1,\"y\":1,\"z\":1}}";
            var deSerialize = new JsonDeSerializer().DeSerialize<Vector3>(json);
            Debug.Assert(deSerialize == val, "TestVector3JsonDeSerialize Failed!!!");
        }

        [Test]
        public void TestQuaternionDeSerialize()
        {
            Quaternion val = Quaternion.identity;
            string json = "{\"Datas\":{\"x\":0,\"y\":0,\"z\":0,\"w\":1}}";
            var deSerialize = new JsonDeSerializer().DeSerialize<Quaternion>(json);
            Debug.Assert(deSerialize == val, "TestQuaternionDeSerialize Failed!!!");
        }

        [Test]
        public void TestColorDeSerialize()
        {
            Color val = Color.green;
            string json = "{\"Datas\":{\"r\":0,\"g\":1,\"b\":0,\"a\":1}}";
            var deSerialize = new JsonDeSerializer().DeSerialize<Color>(json);
            Debug.Assert(deSerialize == val, "TestColorDeSerialize Failed!!!");
        }

        [Test]
        public void TestRectDeSerialize()
        {
            Rect val = new Rect(1, 1, 1, 1);
            string json = "{\"Datas\":{\"x\":1,\"y\":1,\"width\":1,\"height\":1}}";
            var deSerialize = new JsonDeSerializer().DeSerialize<Rect>(json);
            Debug.Assert(deSerialize == val, "TestRectDeSerialize Failed!!!");
        }

    }
}
